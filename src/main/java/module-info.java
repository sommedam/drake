module com.drake.demo {
    exports com.drake.demo.suite01;
    exports com.drake.demo.suite02;
    exports com.drake.demo.suite03;
    exports com.drake.demo.suite04;


    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires junit;
    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires com.almasb.fxgl.all;

    exports com.drake.demo.thedrake.gui.component;
    opens com.drake.demo.gui to javafx.fxml;
    exports com.drake.demo.thedrake;
    opens com.drake.demo.thedrake to javafx.fxml;
    exports com.drake.demo.thedrake.gui.controllers;
    opens com.drake.demo.thedrake.gui.controllers to javafx.fxml;
}