package com.drake.demo.main;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        com.drake.demo.suite01.TestSuite.class,
        com.drake.demo.suite02.TestSuite.class,
        com.drake.demo.suite03.TestSuite.class,
        com.drake.demo.suite04.TestSuite.class
})

public class MainSuite {

}

