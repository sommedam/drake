package com.drake.demo.thedrake;

import java.util.List;

import static com.drake.demo.thedrake.GameResult.DRAW;
import static com.drake.demo.thedrake.GameResult.IN_PLAY;

public class GameState {
    private final Board board;
    private final PlayingSide sideOnTurn;
    private final Army blueArmy;
    private final Army orangeArmy;
    private final GameResult result;

    public GameState(
            Board board,
            Army blueArmy,
            Army orangeArmy) {
        this(board, blueArmy, orangeArmy, PlayingSide.BLUE, IN_PLAY);
    }

    public GameState(
            Board board,
            Army blueArmy,
            Army orangeArmy,
            PlayingSide sideOnTurn,
            GameResult result) {
        this.board = board;
        this.sideOnTurn = sideOnTurn;
        this.blueArmy = blueArmy;
        this.orangeArmy = orangeArmy;
        this.result = result;
    }

        public Board board() {
            return board;
        }

        public String[][] resourcesMatrix() {
            // First, add mountains and such
            String[][] resources = new String[board.dimension()][board.dimension()];
            for (int i = 0; i < board.dimension(); i++) {
                for (int j = 0; j < board.dimension(); j++) {
                    resources[i][j] = tileAt(new BoardPos(board.dimension,i, j)).resourceName();
                }
            }
            // Secondly, add troops
            blueArmy.boardTroops().troopPositions().forEach(pos -> resources[pos.i()][pos.j()] = blueArmy.boardTroops().at(pos).get().resourceName());
            orangeArmy.boardTroops().troopPositions().forEach(pos -> resources[pos.i()][pos.j()] = orangeArmy.boardTroops().at(pos).get().resourceName());

            return resources;
        }

    public PlayingSide sideOnTurn() {
        return sideOnTurn;
    }

    public GameResult result() {
        // Stav hry se zjistuje, pri skoku k dalsi armade.
        // Proto pokud protejsi hrac dal spatne nekam guarda, potom
        boolean isPlacingGuards = armyNotOnTurn().boardTroops().isPlacingGuards();
        if (isPlacingGuards) {
            int jumpPositions = 0;
            for (TilePos pos : armyNotOnTurn().boardTroops().leaderPosition().neighbours()) {
                if (tileAt(pos).canStepOn()) {
                    jumpPositions++;
                }
            }
            if (jumpPositions == 0) {
                // Pokud se hrac sekne pri staveni, tak je to remiza
                return DRAW;
            }
        }

        return result;
    }

    public Army army(PlayingSide side) {
        if (side == PlayingSide.BLUE) {
            return blueArmy;
        }

        return orangeArmy;
    }

    public Army armyOnTurn() {
        return army(sideOnTurn);
    }

    public Army armyNotOnTurn() {
        if (sideOnTurn == PlayingSide.BLUE)
            return orangeArmy;

        return blueArmy;
    }

    // Vrátí dlaždici, která se nachází na hrací desce na pozici pos.
    // Musí tedy zkontrolovat, jestli na této pozici není jednotka z
    // armády nějakého hráče a pokud ne, vrátí dlaždici z objektu board
    public Tile tileAt(TilePos pos) {
        // Place for your code
        if (blueArmy.boardTroops().at(pos).isPresent()) {
            return blueArmy.boardTroops().at(pos).get();
        } else if (orangeArmy.boardTroops().at(pos).isPresent()) {
            return orangeArmy.boardTroops().at(pos).get();
        } else {
            return board.at(pos);
        }
    }

    private boolean bothArmiesBuilt(){
        return blueArmy.boardTroops().isLeaderPlaced() && !blueArmy.boardTroops().isPlacingGuards() &&
                orangeArmy.boardTroops().isLeaderPlaced() && !orangeArmy.boardTroops().isPlacingGuards();
    }

    // Vrátí true, pokud je možné ze zadané pozice začít tah nějakou
    // jednotkou. Vrací false, pokud stav hry není IN_PLAY, pokud
    // na dané pozici nestojí žádná jednotka nebo pokud na pozici
    // stojí jednotka hráče, který zrovna není na tahu.
    // Při implementaci vemte v úvahu zahájení hry. Dokud nejsou
    // postaveny stráže, žádné pohyby jednotek po desce nejsou možné.
    private boolean canStepFrom(TilePos origin) {
        // Place for your code
        return (result.equals(IN_PLAY) &&
                armyOnTurn(). boardTroops().at(origin).isPresent() &&
                bothArmiesBuilt());
    }

    // Vrátí true, pokud je možné na zadanou pozici dokončit tah nějakou
    // jednotkou. Vrací false, pokud stav hry není IN_PLAY nebo pokud
    // na zadanou dlaždici nelze vstoupit (metoda Tile.canStepOn).
    private boolean canStepTo(TilePos target) {
        // Place for your code
        return (result.equals(IN_PLAY) &&
                !target.equals(TilePos.OFF_BOARD) &&
                // Not via Board, it doesnt consider troops
                tileAt(target).canStepOn());
    }

    // Vrátí true, pokud je možné na zadané pozici vyhodit soupeřovu jednotku.
    // Vrací false, pokud stav hry není IN_PLAY nebo pokud
    // na zadané pozici nestojí jednotka hráče, který zrovna není na tahu.
    private boolean canCaptureOn(TilePos target) {
        // Place for your code
        return result == IN_PLAY &&
                armyNotOnTurn().boardTroops().at(target).isPresent();
    }

    public boolean canStep(TilePos origin, TilePos target) {
        return canStepFrom(origin) && canStepTo(target);
    }

    public boolean canCapture(TilePos origin, TilePos target) {
        return canStepFrom(origin) && canCaptureOn(target);
    }


    // Vrátí true, pokud je možné na zadanou pozici položit jednotku ze
    // zásobníku.. Vrací false, pokud stav hry není IN_PLAY, pokud je zásobník
    // armády, která je zrovna na tahu prázdný, pokud není možné na danou
    // dlaždici vstoupit. Při implementaci vemte v úvahu zahájení hry, kdy
    // se vkládání jednotek řídí jinými pravidly než ve střední hře.
    public boolean canPlaceFromStack(TilePos target) {
        // Place for your code
        Army army = armyOnTurn();
        BoardTroops boardTroops = army.boardTroops();

        if (army.stack().isEmpty()) {
            return false;
        } else if (!canStepTo(target)) {
            return false;
        } else if (boardTroops.isPlacingGuards()) {
            return target.isNextTo(boardTroops.leaderPosition());
        } else if (!boardTroops.isLeaderPlaced()) {
            if (army == blueArmy) {
                // Blue has to be on top
                return target.row() == 1;
            } else if (army == orangeArmy) {
                // Orange is at the bottom
                return target.row() == board.dimension();
            }
        }

        // Or if target is near other friendly troops
        for (BoardPos pos : boardTroops.troopPositions()) {
            if (target.isNextTo(pos)) {
                return true;
            }
        }
        return false;
    }

    public GameState stepOnly(BoardPos origin, BoardPos target) {
        if (canStep(origin, target))
            return createNewGameState(
                    armyNotOnTurn(),
                    armyOnTurn().troopStep(origin, target), IN_PLAY);

        throw new IllegalArgumentException();
    }

    public GameState stepAndCapture(BoardPos origin, BoardPos target) {
        if (canCapture(origin, target)) {
            Troop captured = armyNotOnTurn().boardTroops().at(target).get().troop();
            GameResult newResult = IN_PLAY;

            if (armyNotOnTurn().boardTroops().leaderPosition().equals(target))
                newResult = GameResult.VICTORY;

            return createNewGameState(
                    armyNotOnTurn().removeTroop(target),
                    armyOnTurn().troopStep(origin, target).capture(captured), newResult);
        }

        throw new IllegalArgumentException();
    }

    public GameState captureOnly(BoardPos origin, BoardPos target) {
        if (canCapture(origin, target)) {
            Troop captured = armyNotOnTurn().boardTroops().at(target).get().troop();
            GameResult newResult = IN_PLAY;

            if (armyNotOnTurn().boardTroops().leaderPosition().equals(target))
                newResult = GameResult.VICTORY;

            return createNewGameState(
                    armyNotOnTurn().removeTroop(target),
                    armyOnTurn().troopFlip(origin).capture(captured), newResult);
        }

        throw new IllegalArgumentException();
    }

    public GameState placeFromStack(BoardPos target) {
        if (canPlaceFromStack(target)) {
            return createNewGameState(
                    armyNotOnTurn(),
                    armyOnTurn().placeFromStack(target),
                    IN_PLAY);
        }

        throw new IllegalArgumentException();
    }

    public GameState resign() {
        return createNewGameState(
                armyNotOnTurn(),
                armyOnTurn(),
                GameResult.VICTORY);
    }

    public GameState draw() {
        return createNewGameState(
                armyOnTurn(),
                armyNotOnTurn(),
                GameResult.DRAW);
    }



    private GameState createNewGameState(Army armyOnTurn, Army armyNotOnTurn, GameResult result) {
        if (armyOnTurn.side() == PlayingSide.BLUE) {
            return new GameState(board, armyOnTurn, armyNotOnTurn, PlayingSide.BLUE, result);
        }

        return new GameState(board, armyNotOnTurn, armyOnTurn, PlayingSide.ORANGE, result);
    }
}
