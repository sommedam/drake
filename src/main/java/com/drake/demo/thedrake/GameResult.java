package com.drake.demo.thedrake;

public enum GameResult {
    VICTORY, DRAW, IN_PLAY;
}
