package com.drake.demo.thedrake;

import java.util.*;


public class BoardTroops {
    private final PlayingSide playingSide;
    private final Map<BoardPos, TroopTile> troopMap;
    private final TilePos leaderPosition;
    private final int troopsCount;
    private final boolean guardsPlaced;

    public BoardTroops(PlayingSide playingSide) {
        // Místo pro váš kód
        this.playingSide = playingSide;
        this.troopMap = Collections.emptyMap();
        this.leaderPosition = TilePos.OFF_BOARD;
        this.troopsCount = 0;
        this.guardsPlaced = false;
    }

    public BoardTroops(
            PlayingSide playingSide,
            Map<BoardPos, TroopTile> troopMap,
            TilePos leaderPosition,
            int troopsCount, boolean guardsPlaced) {
        // Místo pro váš kód
        this.playingSide = playingSide;
        this.troopMap = troopMap;
        this.leaderPosition = leaderPosition;
        this.troopsCount = troopsCount;
        this.guardsPlaced = guardsPlaced;
    }

    public Optional<TroopTile> at(TilePos pos) {
        // Místo pro váš kód
        return Optional.ofNullable(troopMap.get(pos));
    }

    public PlayingSide playingSide() {
        return playingSide;
    }

    public TilePos leaderPosition() {
        return leaderPosition;
    }

    public int guards() {
        return troopsCount;
    }

    public boolean isLeaderPlaced() {
        return (this.leaderPosition != TilePos.OFF_BOARD);
    }

    public boolean isPlacingGuards() {
        return !guardsPlaced && isLeaderPlaced();
    }

    private boolean isBoardPositionOccupied(BoardPos target) {
        return (this.troopMap.containsKey(target) || this.leaderPosition == target);
    }

    public Set<BoardPos> troopPositions() {
        return troopMap.keySet();
    }

    public BoardTroops placeTroop(Troop troop, BoardPos target) {
        // Místo pro váš kód
        if (isBoardPositionOccupied(target)) {
            throw new IllegalArgumentException();
        }
        TilePos newLeaderPosition = leaderPosition;
        int newGuards = troopsCount;
        if (!isLeaderPlaced()) {
            newLeaderPosition = target;
        }else {
            if(isPlacingGuards()) {
                newGuards++;
            }
        }
        Map<BoardPos, TroopTile> newMap = new HashMap<>(troopMap);
        newMap.put(target, new TroopTile(troop, playingSide, TroopFace.AVERS));
        return new BoardTroops(playingSide, newMap, newLeaderPosition, newGuards, (guardsPlaced || newGuards == 2));
    }

    public BoardTroops troopStep(BoardPos origin, BoardPos target) {
        // Místo pro váš kód
        boolean placingGuards = isPlacingGuards();
        boolean leaderPlaced = isLeaderPlaced();
        boolean occupied = isBoardPositionOccupied(target);
        if (placingGuards || !leaderPlaced) {
            throw new IllegalStateException();
        }
        if (!troopMap.containsKey(origin) || occupied) {
            throw new IllegalArgumentException();
        }

        TroopTile movingTroopOfTile = troopMap.get(origin);
        TroopFace newFace;
        TilePos newLeaderPosition = leaderPosition;

        if (origin.equals(leaderPosition)) {
            newLeaderPosition = target;
        }

        if (movingTroopOfTile.face() == TroopFace.AVERS) {
            newFace = TroopFace.REVERS;
        } else {
            newFace = TroopFace.AVERS;
        }

        TroopTile newTroopTile = new TroopTile(movingTroopOfTile.troop(), movingTroopOfTile.side(), newFace);
        Map<BoardPos, TroopTile> newMap = new HashMap<>(troopMap);
        newMap.put(target, newTroopTile);
        newMap.remove(origin);
        return new BoardTroops(playingSide, newMap, newLeaderPosition, troopsCount, (guardsPlaced || troopsCount == 2));
    }

    public BoardTroops troopFlip(BoardPos origin) {
        if (!isLeaderPlaced()) {
            throw new IllegalStateException(
                    "Cannot move troops before the leader is placed.");
        }

        if (isPlacingGuards()) {
            throw new IllegalStateException(
                    "Cannot move troops before guards are placed.");
        }

        if (!at(origin).isPresent())
            throw new IllegalArgumentException();

        Map<BoardPos, TroopTile> newTroops = new HashMap<>(troopMap);
        TroopTile tile = newTroops.remove(origin);
        newTroops.put(origin, tile.flipped());

        return new BoardTroops(playingSide(), newTroops, leaderPosition, troopsCount, guardsPlaced);
    }

    public BoardTroops removeTroop(BoardPos target) {
        // Místo pro váš kód
        if (!isLeaderPlaced() || isPlacingGuards()) {
            throw new IllegalStateException();
        }
        if (!troopMap.containsKey(target)) {
            throw new IllegalArgumentException();
        }
        TilePos newLeaderPosition = leaderPosition;
        int newGuards= troopsCount;
        if (target.equals(leaderPosition)) {
            newLeaderPosition = (TilePos.OFF_BOARD);
        }else{
            newGuards--;
        }
        Map<BoardPos, TroopTile> newMap = new HashMap<>(troopMap);
        newMap.remove(target);
        return new BoardTroops(playingSide, newMap, newLeaderPosition, newGuards, (guardsPlaced || newGuards == 2));
    }
}
