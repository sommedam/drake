package com.drake.demo.thedrake;

import java.util.ArrayList;
import java.util.List;

public class TroopTile implements Tile {
    private final Troop troop;
    private final PlayingSide side;
    private final TroopFace face;

    // Konstruktor
    public TroopTile(Troop troop, PlayingSide side, TroopFace face){
        this.troop = troop;
        this.side = side;
        this.face = face;
    }

    // Vrací barvu, za kterou hraje jednotka na této dlaždici
    public PlayingSide side(){
        return side;
    }

    // Vrací stranu, na kterou je jednotka otočena
    public TroopFace face(){
        return face;
    }

    // Jednotka, která stojí na této dlaždici
    public Troop troop(){
        return troop;
    }

    @Override
    public String resourceName() {
        return "front" + troop.name() + (side == PlayingSide.BLUE ? "B" : "O");
    }

    // Vrací False, protože na dlaždici s jednotkou se nedá vstoupit
    public boolean canStepOn(){
        return false;
    }

    // Vrací True
    public boolean hasTroop(){
        return true;
    }

    @Override
    public List<Move> movesFrom(BoardPos pos, GameState state) {
        List<Move> resultNew = new ArrayList<>();
        // Projde všechny akce na správné straně jednotky a sjednotí jimi vrácené tahy do jednoho seznamu.
        for (TroopAction action : troop.actions(face))
            for (Move move : action.movesFrom(pos, side, state)) {
                // Debug
                resultNew.add(move);
            }
        return resultNew;
    }

    // Vytvoří novou dlaždici, s jednotkou otočenou na opačnou stranu
    // (z rubu na líc nebo z líce na rub)
    public TroopTile flipped() {
        TroopFace flippedFace;
        if(face == TroopFace.AVERS){
            flippedFace = TroopFace.REVERS;
        }else{
            flippedFace = TroopFace.AVERS;
        }
        return new TroopTile(troop,side,flippedFace);
    }
}
