package com.drake.demo.thedrake;

import java.util.ArrayList;
import java.util.List;

public class SlideAction extends TroopAction {

    public SlideAction(Offset2D offset) {
        super(offset);
    }

    public SlideAction(int offsetX, int offsetY) {
        super(offsetX, offsetY);
    }


    /*
    * List all moves possible from given TilePosition and player state.
    * */
    @Override
    public List<Move> movesFrom(BoardPos origin, PlayingSide side, GameState state) {
        List<Move> result = new ArrayList<>();
        // Only TilePos instance, without further logic
        // Offset is move orientation to Player
        TilePos target = origin.stepByPlayingSide(offset(), side);
        int offsetMultiplier = 1;
        while (!target.equals(TilePos.OFF_BOARD)) {
            offsetMultiplier++;
            // If we can just step from origin position to target
            if (state.canStep(origin, target))
                result.add(new StepOnly(origin, (BoardPos) target));
            else if (state.canCapture(origin, target)) {
                result.add(new StepAndCapture(origin, (BoardPos) target));
                // If troop can step to some other troop, that means, any further Tile will be inaccessible
                break;
            }
            else
                break;

            target = origin.stepByPlayingSide(new Offset2D(offset().x * offsetMultiplier,offset().y * offsetMultiplier), side);
        }

        return result;
    }
}