package com.drake.demo.thedrake.gui.component;

import com.drake.demo.thedrake.GameState;
import com.drake.demo.thedrake.PlayingSide;
import com.drake.demo.thedrake.gui.controllers.EventBundle;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class BinComponent extends VBox implements IGameStateChangeObserver {
    PlayingSide side;

    public void setSide(PlayingSide side) {
        this.side = side;
    }

    @Override
    public void updateGameState(GameState gameState, EventBundle previousEvent) {
        getChildren().clear();
        if(!gameState.army(side).captured().isEmpty()) {
            ImageView skullImageView = new ImageView();
            String icoName = side == PlayingSide.ORANGE ? "b_skull" : "o_skull";
            Image skullImage = new Image(getClass().getResourceAsStream("/com/drake/demo/gui/" + icoName + ".png"));
            skullImageView.setImage(skullImage);
            skullImageView.setFitWidth(40);
            skullImageView.setFitHeight(30);
            getChildren().add(skullImageView);
        }

        gameState.army(side).captured().forEach(troop -> {
            Text text = new Text(troop.name());
            text.setStyle("-fx-text-fill: '00bfff'; -fx-font-size: 16px;");
            getChildren().add(text);
        });
    }
}
