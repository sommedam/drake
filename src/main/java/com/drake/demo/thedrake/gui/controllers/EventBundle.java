package com.drake.demo.thedrake.gui.controllers;

import com.drake.demo.thedrake.GameState;
import com.drake.demo.thedrake.gui.component.ISelectable;

public class EventBundle {
    public ISelectable selectable;

    // If event selected given item
    public boolean selected;
    public GameState proposedGameState;

    public EventBundle(ISelectable selectable, boolean selected, GameState proposedGameState){
        this.selectable = selectable;
        this.selected = selected;
        this.proposedGameState = proposedGameState;

    }
}
