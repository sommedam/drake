package com.drake.demo.thedrake.gui.controllers;

public interface IOnClickListener {
    void getEventBundle(EventBundle eventBundle);
}
