package com.drake.demo.thedrake.gui.controllers;

import com.drake.demo.thedrake.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

public class MainMenuController {

    @FXML
    public Text the_drake_text;

    @FXML
    private void handleTwoPlayers(ActionEvent event) throws IOException {
        jumpSplitScreen(event);
    }

    public void jumpSplitScreen(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("/com/drake/demo/gui/split_screen.fxml"));
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Scene scene = new Scene(fxmlLoader.load(), 720, 720);
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/com/drake/demo/gui/split_screen.css")).toExternalForm());
        stage.setScene(scene);
        stage.setTitle("The Drake");
        stage.show();
    }

    @FXML
    private void handleAgainstComputer(ActionEvent event) {
    }

    @FXML
    private void handleOnlineGame(ActionEvent event) {
    }

    @FXML
    private void handleExit(ActionEvent event) {
        System.exit(0);
    }
}