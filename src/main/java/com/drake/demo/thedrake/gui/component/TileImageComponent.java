package com.drake.demo.thedrake.gui.component;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.util.Objects;


public class TileImageComponent extends StackPane {

    public TileImageComponent(Background background, String id, EventHandler<MouseEvent> onMouseClicked) {
        setId(id);
        //setBackground(background);
        ImageView baseImage = new ImageView();
        Image backgroundImg = background.getImages().get(0).getImage();
        baseImage.setImage(backgroundImg);
        baseImage.setFitHeight(70);
        baseImage.setFitWidth(70);
        baseImage.setId(id);
        baseImage.setOnMouseClicked(onMouseClicked);
        getChildren().add(baseImage);
        /*
        try {
            if (baseTilePath != null) {
                Image cellImage = new Image(Objects.requireNonNull(getClass().getResource(baseTilePath)).toString());
                baseImage.setImage(cellImage);
                baseImage.setOnMouseClicked(onMouseClicked);
            }
        } catch (Exception e) {
            System.err.println(baseTilePath);
            //e.printStackTrace();
        }
        getChildren().add(baseImage);
        */
    }

    public void appendOverlay(String overlayTilePath, String id, EventHandler<MouseEvent> onMouseClicked) {
        Image frameImage = new Image(Objects.requireNonNull(getClass().getResource(overlayTilePath)).toString());
        ImageView frame = new ImageView();
        frame.setImage(frameImage);
        frame.setId(id);
        frame.setOnMouseClicked(onMouseClicked);
        frame.setFitHeight(70);
        frame.setFitWidth(70);
        getChildren().add(frame);
    }
}
