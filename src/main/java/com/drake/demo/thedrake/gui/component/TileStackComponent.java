package com.drake.demo.thedrake.gui.component;

import com.drake.demo.thedrake.GameState;
import com.drake.demo.thedrake.PlayingSide;
import com.drake.demo.thedrake.gui.controllers.EventBundle;
import com.drake.demo.thedrake.gui.controllers.IOnClickListener;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import java.util.Objects;

import static com.drake.demo.thedrake.PlayingSide.BLUE;

public class TileStackComponent extends HBox implements IGameStateChangeObserver, ISelectable, EventHandler<MouseEvent> {
    private PlayingSide side;
    boolean selected;
    IOnClickListener onClickListener;
    GameState gameState;

    EventBundle previousEventBundle;

    public void setClickListener(IOnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public void setSide(PlayingSide side) {
        this.side = side;
    }

    @Override
    public void handle(MouseEvent event) {
        GameState gameStateProposition = gameState;
        if(gameState.armyOnTurn().side() == side && !getChildren().isEmpty()){
            // If can click on queue activate it
            onClickListener.getEventBundle(new EventBundle(this, true, gameStateProposition));
        }
    }

    @Override
    public void updateGameState(GameState gameState, EventBundle previousEvent) {
        this.gameState = gameState;
        this.previousEventBundle = previousEvent;
        this.getChildren().clear();
        updateStack();
    }

    private void updateStack() {
        getChildren().clear();
        gameState.army(side).stack().forEach(troop -> {
            StackPane cell = new StackPane();
            ImageView imageView = new ImageView();
            imageView.setFitHeight(55);
            imageView.setFitWidth(55);
            String resName = "front" + troop.name() + (side == BLUE ? "B" : "O");
            Image cellImage = new Image(Objects.requireNonNull(getClass().getResource("/com/drake/demo/gui/tiles/" + resName + ".png")).toString());
            imageView.setImage(cellImage);
            ImageView frame = new ImageView();
            // If can click on queue activate it, instead of only clicking leading item
            imageView.setOnMouseClicked(this);
            if (getChildren().isEmpty()) {
                imageView.setId("front_" + (side == BLUE ? "blue" : "orange"));

                // If rewriting this stack after clicking
                if (previousEventBundle != null && previousEventBundle.selected && Objects.equals(previousEventBundle.selectable.getNode().getId(), getId())) {
                    frame.setFitHeight(55);
                    frame.setFitWidth(55);
                    String frameRes = "selected.png";
                    Image frameImage = new Image(Objects.requireNonNull(getClass().getResource("/com/drake/demo/gui/tiles/" + frameRes)).toString());
                    frame.setImage(frameImage);
                }
            }
            cell.getChildren().addAll(imageView, frame);
            getChildren().add(cell);
        });
    }

    @Override
    public Node getNode() {
        return this;
    }

    @Override
    public void quitSelect() {
        selected = false;
    }
}
