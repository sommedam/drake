package com.drake.demo.thedrake.gui.component;

import com.drake.demo.thedrake.GameState;
import com.drake.demo.thedrake.gui.controllers.EventBundle;

public interface IGameStateChangeObserver {
    void updateGameState(GameState gameState, EventBundle previousEvent);
}
