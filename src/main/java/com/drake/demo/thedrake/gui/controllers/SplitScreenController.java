package com.drake.demo.thedrake.gui.controllers;

import com.drake.demo.thedrake.*;
import com.drake.demo.thedrake.gui.component.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.Random;
import java.util.ResourceBundle;

import static com.drake.demo.thedrake.PlayingSide.BLUE;
import static com.drake.demo.thedrake.PlayingSide.ORANGE;


public class SplitScreenController implements Initializable, IOnClickListener {
    GameState gameState;

    @FXML
    public StackPane layout;
    @FXML
    public EndingFragmentComponent ending_fragment;
    @FXML
    public BinComponent bin_blue;
    @FXML
    public BinComponent bin_orange;
    @FXML
    public StateLabelComponent state_label;
    @FXML
    private BoardGridComponent grid;
    @FXML
    private TileStackComponent q_orange;
    @FXML
    private TileStackComponent q_blue;

    @FXML
    void navigateToMainMenu(ActionEvent event) throws IOException {
        // Get the stage from the event source
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("/com/drake/demo/gui/main_menu.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 720, 720);
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/com/drake/demo/gui/css.css")).toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void resetGame(ActionEvent event) throws IOException {
        setupGame();
    }

    void updateLayout() {
        final IGameStateChangeObserver[] observers =
                {grid, q_blue, q_orange, state_label, bin_blue, bin_orange, ending_fragment};
        for (IGameStateChangeObserver observer : observers) {
            observer.updateGameState(gameState, lastEventBundle);
        }
    }

    static SelectState getSelectState(GameState gameState) {
        if (gameState.armyOnTurn().boardTroops().isPlacingGuards() ||
                gameState.armyNotOnTurn().boardTroops().isPlacingGuards()) {
            return SelectState.PLACE_GUARDS;
        } else if (!gameState.armyOnTurn().boardTroops().isLeaderPlaced() ||
                !gameState.armyNotOnTurn().boardTroops().isLeaderPlaced()) {
            return SelectState.PLACING_LEADER;
        }
        return SelectState.FREE_SELECT;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Image img = new Image((Objects.requireNonNull(getClass().getResourceAsStream("/com/drake/demo/gui/ui_background.png"))));
        Background boardBackground = new Background(
                new BackgroundImage(img, BackgroundRepeat.SPACE, BackgroundRepeat.SPACE, null, null));
        layout.setBackground(boardBackground);
        setupGame();
    }

    void setupGame(){
        StandardDrakeSetup setup = new StandardDrakeSetup();
        Board board = new Board(4);
        // <0,3>
        int numberOfMountains = new Random().nextInt(4);
        for (int i = 0; i < numberOfMountains; i++) {
            board = board.withTiles(new Board.TileAt(board.positionFactory().pos(new Random().nextInt(4), new Random().nextInt(4)), BoardTile.MOUNTAIN));
        }
        gameState = setup.startState(board);

        q_blue.setSide(BLUE);
        q_orange.setSide(ORANGE);
        q_blue.setClickListener(this);
        q_orange.setClickListener(this);

        bin_blue.setSide(BLUE);
        bin_orange.setSide(ORANGE);

        grid.setClickListener(this);

        // Set initial focus state
        lastEventBundle = null;
        getEventBundle(new EventBundle(q_blue, true, gameState));
    }

    // For keeping track of last selected item
    EventBundle lastEventBundle;
    @Override
    public void getEventBundle(EventBundle eventBundle) {
        gameState = eventBundle.proposedGameState;
        // Unselect last selected item
        if(lastEventBundle!=null && eventBundle.selectable.getNode() != lastEventBundle.selectable.getNode()){
            lastEventBundle.selectable.quitSelect();
        }

        // Autoselects
        if(getSelectState(gameState) != SelectState.FREE_SELECT) {
            switch (gameState.armyOnTurn().side()){
                // Rewrite waiting state, move deselects all
                case BLUE -> lastEventBundle = new EventBundle(q_blue, true, eventBundle.proposedGameState);
                case ORANGE -> lastEventBundle = new EventBundle(q_orange, true, eventBundle.proposedGameState);
            }
        }else{
            lastEventBundle = eventBundle;
        }

        updateLayout();
    }

    // For UX purposes, during placing of known Tiles, selecting is automatic
    enum SelectState {
        PLACING_LEADER,
        PLACE_GUARDS,
        FREE_SELECT
    }
}