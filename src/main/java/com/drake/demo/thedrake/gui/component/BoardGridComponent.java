package com.drake.demo.thedrake.gui.component;

import com.drake.demo.thedrake.*;
import com.drake.demo.thedrake.gui.controllers.EventBundle;
import com.drake.demo.thedrake.gui.controllers.IOnClickListener;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import kotlin.Pair;

import java.util.List;
import java.util.Objects;

import static com.drake.demo.thedrake.PlayingSide.BLUE;

public class BoardGridComponent extends GridPane implements IGameStateChangeObserver, ISelectable, EventHandler<MouseEvent> {
    private IOnClickListener onClickListener;
    private GameState gameState;
    private Pair<Integer, Integer> lastSelected;
    private EventBundle previousEventBundle;

    private BoardPos inputToGameStatePos(int x, int y) {
        return new BoardPos(4, x, 3 - y);
    }

    @Override
    public void handle(MouseEvent event) {
        EventBundle returnBundle = new EventBundle(this, false, gameState);

        GameState gameStateProposition = gameState;
        if (event.getSource() instanceof ImageView cell) {
            int x = GridPane.getColumnIndex(cell.getParent());
            int y = GridPane.getRowIndex(cell.getParent());
            if (Objects.equals(cell.getId(), "move")) {
                // If have selected something, move it
                if (previousEventBundle.selected) {
                    // If selected TileStackComponent, place from stack
                    if (previousEventBundle.selectable.getNode() instanceof TileStackComponent) {
                        gameStateProposition = gameStateProposition.placeFromStack(inputToGameStatePos(x, y));
                    } else if (previousEventBundle.selectable == this) {
                        BoardPos origin = inputToGameStatePos(lastSelected.getFirst(), lastSelected.getSecond());
                        BoardPos target = inputToGameStatePos(x, y);
                        /*System.out.println("ORIGIN");
                        System.out.println("x: " + lastSelected.getFirst() + " y: " + lastSelected.getSecond());
                        System.out.println("-> x: " + origin.i() + " y: " + origin.j());
                        System.out.println("TARGET");
                        System.out.println("x: " + x + " y: " + y);
                        System.out.println("-> x: " + target.i() + " y: " + target.j());
                        System.out.println("Moving from " + origin + " to " + target);
                        */
                        if (gameStateProposition.canCapture(origin, target)) {
                            // If Troop can capture at that Tile
                            gameStateProposition = gameStateProposition.stepAndCapture(origin, target);
                        } else {
                            // If Troop can move at that Tile
                            gameStateProposition = gameStateProposition.stepOnly(origin, target);
                        }
                    }
                }

                // If moved, none Tile is selected
                returnBundle = new EventBundle(this, false, gameStateProposition);
            } else if (Objects.equals(cell.getId(), "cell" + x + y) &&
                    // If there is a troop of color on selected cell
                    gameState.armyOnTurn().boardTroops().at(inputToGameStatePos(x, y)).isPresent()
            ) {
                System.out.println("Selected: " + x + " " + y);
                returnBundle = new EventBundle(this, true, gameStateProposition);
            }
            lastSelected = new Pair<>(x, y);
            onClickListener.getEventBundle(returnBundle);
        }
    }

    public void setClickListener(IOnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void updateGameState(GameState gameState, EventBundle previousEvent) {
        this.gameState = gameState;
        this.previousEventBundle = previousEvent;

        getChildren().clear();

        boolean[][] possibleMove = generatePossibleMoves(gameState);

        TileBackgrounds tileBackgrounds = new TileBackgrounds();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                BoardPos boardPos = inputToGameStatePos(i, j);
                Background tileBackground = tileBackgrounds.get(gameState.tileAt(boardPos));

                if(gameState.tileAt(boardPos).resourceName()!=null){
                    System.out.println("Tile tex: " + gameState.tileAt(boardPos).resourceName());
                }

                TileImageComponent tileImageComponent = new TileImageComponent(tileBackground, "cell" + i + j, this);

                if (previousEventBundle.selected && previousEventBundle.selectable == this &&
                        lastSelected != null && lastSelected.getFirst() == i && lastSelected.getSecond() == j) {
                    tileImageComponent.appendOverlay("/com/drake/demo/gui/tiles/selected.png", "selected", this);
                }

                // If is possible move add semitransparent overlay
                if (possibleMove[i][j]
                        // Cant jump at own troops
                        && gameState.armyOnTurn().boardTroops().at(new BoardPos(4, i, 3 - j)).isEmpty()) {
                    tileImageComponent.appendOverlay("/com/drake/demo/gui/tiles/move.png", "move", this);
                }
                add(tileImageComponent, i, j);
            }
        }
    }

    private boolean[][] generatePossibleMoves(GameState gameState) {
        boolean[][] possibleMoves = new boolean[4][4];
        ValidMoves validMoves = new ValidMoves(gameState);

        // STACK placing rules
        if(previousEventBundle.selectable.getNode() instanceof TileStackComponent){
            // If placing TROOPS
            List<Move> movesFromStack = validMoves.movesFromStack();
            movesFromStack.forEach(move -> {
                if(gameState.board().at(move.target()).canStepOn()){
                    possibleMoves[move.target().i()][3 - move.target().j()] = true;
                }
            });
        }else if(previousEventBundle.selectable == this){
            List<Move> movesFromSelected = validMoves.boardMoves(inputToGameStatePos(lastSelected.getFirst(), lastSelected.getSecond()));
            movesFromSelected.forEach(move -> {
                if(gameState.board().at(move.target()).canStepOn()){
                possibleMoves[move.target().i()][3 - move.target().j()] = true;
            }});
        }
        return possibleMoves;
    }

    @Override
    public Node getNode() {
        return this;
    }

    @Override
    public void quitSelect() {
        // selected = false;
    }
}
