package com.drake.demo.thedrake.gui.component;

import javafx.scene.Node;

public interface ISelectable {
    Node getNode();

    // Item selects itself, based on inner logic
    // void select();
    void quitSelect();
}
