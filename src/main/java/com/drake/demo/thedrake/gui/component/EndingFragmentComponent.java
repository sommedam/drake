package com.drake.demo.thedrake.gui.component;

import com.drake.demo.thedrake.GameResult;
import com.drake.demo.thedrake.GameState;
import com.drake.demo.thedrake.PlayingSide;
import com.drake.demo.thedrake.gui.controllers.EventBundle;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class EndingFragmentComponent extends StackPane implements IGameStateChangeObserver {
    @Override
    public void updateGameState(GameState gameState, EventBundle previousEvent) {
        this.setVisible(gameState.result() == GameResult.VICTORY || gameState.result() == GameResult.DRAW);
        lookupAll("#ending_fragment_headline").forEach(node -> {
            String text = "";
            if(gameState.result() == GameResult.VICTORY) {
                text+="VÝHRÁL " + (gameState.sideOnTurn() == PlayingSide.BLUE ? "ORANŽOVÝ " : "MODRÝ ") + " HRÁČ";
            }else {
                text+="REMÍZA";
            }
            ((Text) node).setText(text);
        });
        setBackground(new Background(
                new BackgroundFill(new Color(0, 0.08, 0, 0.96), null, null)));



    }
}
