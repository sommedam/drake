package com.drake.demo.thedrake.gui.component;

import com.drake.demo.thedrake.GameState;
import com.drake.demo.thedrake.PlayingSide;
import com.drake.demo.thedrake.gui.controllers.EventBundle;
import javafx.scene.control.Label;

public class StateLabelComponent extends Label implements IGameStateChangeObserver{

    @Override
    public void updateGameState(GameState gameState, EventBundle previousEvent) {
        setTextFill(javafx.scene.paint.Color.WHITE);
        setFont(new javafx.scene.text.Font("Arial", 30));
        String state = " ";
        if(gameState.armyOnTurn().side() == PlayingSide.BLUE){
            state += "Modrý ";
        } else {
            state += "Oranžový ";
        }
        state+="hráč je na řadě";
        setText(state);
    }
}
