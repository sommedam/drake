package com.drake.demo.thedrake;


public class Board {
    final int dimension;
    final BoardTile[][] board;

    // Konstruktor. Vytvoří čtvercovou hrací desku zadaného rozměru, kde všechny dlaždice jsou prázdné, tedy BoardTile.EMPTY
    public Board(int dimension) {
        this.dimension = dimension;
        this.board = generateEmptyBoard(dimension);

    }

    public BoardTile[][] generateEmptyBoard(int dimensions){
        BoardTile[][] generated = new BoardTile[dimensions][dimensions];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                generated[i][j] = BoardTile.EMPTY;
            }
        }
        return generated;
    }

    // Konstruktor. Vytvoří čtvercovou hrací desku zadaného rozměru, kde všechny dlaždice jsou prázdné, tedy BoardTile.EMPTY
    public Board(int dimension, BoardTile[][] board) {
        this.dimension = dimension;
        this.board = board;
    }

    // Rozměr hrací desky
    public int dimension() {
        return this.dimension;
    }

    // Vrací dlaždici na zvolené pozici.
    public BoardTile at(TilePos pos) {
        return board[pos.i()][pos.j()];
    }

    // Vytváří novou hrací desku s novými dlaždicemi. Všechny ostatní dlaždice zůstávají stejné
    public Board withTiles(TileAt... ats) {
        BoardTile[][] boardTilesReplaced = new BoardTile[dimension][dimension];
        for (int i = 0; i<dimension;i++){
            System.arraycopy(this.board[i], 0, boardTilesReplaced[i], 0, dimension);
        }
        for (TileAt tileReplacement : ats) {
            boardTilesReplaced[tileReplacement.pos.i()][tileReplacement.pos.j()] = tileReplacement.tile;
        }
        return new Board(this.dimension, boardTilesReplaced);
    }

    // Vytvoří instanci PositionFactory pro výrobu pozic na tomto hracím plánu
    public PositionFactory positionFactory() {
        // Místo pro váš kód
        return new PositionFactory(dimension);
    }

    public static class TileAt {
        public final BoardPos pos;
        public final BoardTile tile;

        public TileAt(BoardPos pos, BoardTile tile) {
            this.pos = pos;
            this.tile = tile;
        }
    }
}

