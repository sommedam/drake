package com.drake.demo.thedrake;

import java.util.ArrayList;
import java.util.List;

public class Troop {
    final String name;
    final Offset2D aversPivot;
    final Offset2D reversPivot;

    final List<TroopAction> aversAction;
    final List<TroopAction> reversAction;

    // Hlavní konstruktor
    public Troop(String name, Offset2D aversPivot, Offset2D reversPivot){
        this.name = name;
        this.aversPivot = aversPivot;
        this.reversPivot = reversPivot;
        this.aversAction = new ArrayList<>();
        this.reversAction = new ArrayList<>();
    }
    public Troop(String name, Offset2D aversPivot, Offset2D reversPivot, List<TroopAction> aversAction, List<TroopAction> reversAction){
        this.name = name;
        this.aversPivot = aversPivot;
        this.reversPivot = reversPivot;
        this.aversAction = aversAction;
        this.reversAction = reversAction;
    }

    // Konstruktor, který nastavuje oba pivoty na stejnou hodnotu
    public Troop(String name, Offset2D pivot){
        this.name = name;
        this.aversPivot = pivot;
        this.reversPivot = pivot;
        this.aversAction = new ArrayList<>();
        this.reversAction = new ArrayList<>();
    }

    // Konstruktor, který nastavuje oba pivoty na hodnotu [1, 1]
    public Troop(String name){
        this.name = name;
        this.aversPivot = new Offset2D(1,1);
        this.reversPivot = new Offset2D(1,1);
        this.aversAction = new ArrayList<>();
        this.reversAction = new ArrayList<>();
    }
    public Troop(String name, List<TroopAction> aversAction, List<TroopAction> reversAction){
        this.name = name;
        this.aversPivot = new Offset2D(1,1);
        this.reversPivot = new Offset2D(1,1);
        this.aversAction = aversAction;
        this.reversAction = reversAction;
    }

    // Vrací seznam akcí pro zadanou stranu jednotky
    public List<TroopAction> actions(TroopFace face){
        if(face==TroopFace.AVERS){
            return aversAction;
        }else{
            return reversAction;
        }
    }

    // Vrací jméno jednotky
    public String name(){
        return this.name;
    }

    // Vrací pivot na zadané straně jednotky
    public Offset2D pivot(TroopFace face){
        if(face == TroopFace.REVERS){
            return reversPivot;
        }else{
            return aversPivot;
        }
    }
}
