package com.drake.demo.thedrake;

public enum PlayingSide {
    ORANGE, BLUE
}