package com.drake.demo.thedrake;

import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.paint.Color;

import java.util.Objects;

public class TileBackgrounds {

    public Background EMPTY_BG (){
        Image img = new Image((getClass().getResourceAsStream("/com/drake/demo/gui/tiles/empty.png")));
        return new Background(
                new BackgroundImage(img, null, null, null, null));
    }
    private final Background mountainBg;

    public TileBackgrounds() {
        Image img = new Image((getClass().getResourceAsStream("/com/drake/demo/gui/tiles/mountain.png")));
        this.mountainBg = new Background(
                new BackgroundImage(img, null, null, null, null));
    }

    public Background get(Tile tile) {
        if (tile.hasTroop()) {
            TroopTile armyTile = ((TroopTile) tile);
            return getTroop(armyTile.troop(), armyTile.side(), armyTile.face());
        }

        if (tile == BoardTile.MOUNTAIN) {
            return mountainBg;
        }

        return EMPTY_BG();
    }

    public Background getTroop(Troop info, PlayingSide side, TroopFace face) {
        TroopImageSet images = new TroopImageSet(info.name());
        BackgroundImage bgImage = new BackgroundImage(
                images.get(side, face), null, null, null, null);

        return new Background(bgImage);
    }
}
