# Game Title

![Game Logo](screenshots/the_drake_logo_large.png)

## Funkce:

- Automatické volení zásobníku, pokud je hráč ve fázi stavění stráží
- Rozpoznání zaseknutí hry, pokud není místo na postavení další střáže
- AI generované pozadí

## Screenshots:

<div>
  <img src="screenshots/menu.png" alt="Screenshot 1" width="200" style="margin-right: 10px;">
  <img src="screenshots/game.png" alt="Screenshot 2" width="200" style="margin-right: 10px;">
  <img src="screenshots/result.png" alt="Screenshot 3" width="200">
</div>
